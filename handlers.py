# coding: utf-8

from tornado.web import RequestHandler, authenticated, HTTPError
from tornado.gen import coroutine, Return, Task
from tornado.websocket import WebSocketHandler
import ujson

from models import User
from settings import SESSION_COOKIE_DAYS, REDIS_CHANNEL


class BaseHandler(RequestHandler):

    @coroutine
    def prepare(self):
        token = self.get_secure_cookie("token")
        self.current_user = yield User.by_token(self.application.redis, token)

    @coroutine
    def _get_token(self):
        self.current_user = yield User.by_login(
            self.application.redis,
            self.get_argument("login", ''),
            self.get_argument("password", '')
        )
        if self.current_user:
            raise Return(self.current_user.token)


class IndexHandler(BaseHandler):
    @authenticated
    def get(self):
        self.render("index.html")


class LoginHandler(BaseHandler):

    def get(self):
        self.render("login.html")

    @coroutine
    def post(self):
        token = yield self._get_token()
        self.set_secure_cookie("token", token,
                               expires_days=SESSION_COOKIE_DAYS)
        self.redirect("/")


class TokenHandler(BaseHandler):

    @coroutine
    def post(self):
        token = yield self._get_token()
        if token:
            self.write(token)
        else:
            raise HTTPError(404)


class WSHandler(WebSocketHandler):

    @coroutine
    def open(self):
        token = self.get_argument("token")
        self.current_user = yield User.by_token(self.application.redis, token)
        if self.current_user:
            self.application.ws[self.current_user.login] = self
            yield self.current_user.activate(self.application.redis)
        else:
            self.close(reason='Invalid token')

    @coroutine
    def on_message(self, message):
        if self.current_user:
            try:
                data = ujson.loads(message)
                if data['cmd'] == 'WHO':
                    res = ujson.dumps({
                        'sender': 'SERVER',
                        'recipient': self.current_user.login,
                        'message': (yield User.who(self.application.redis))
                    })
                    self.write_message(res)
                    raise Return()

                elif data['cmd'] == 'MSG':
                    keys = {'sender', 'recipient', 'message', 'cmd'}
                    if keys == set(data.keys()) and \
                       data['sender'] == self.current_user.login:
                        if data['recipient'] == 'ALL':
                            exists = True
                        else:
                            exists = yield Task(self.application.redis.exists,
                                                'user:%s' % data['recipient'])
                        if exists:
                            yield Task(self.application.redis.publish,
                                       REDIS_CHANNEL, message)
                            raise Return()

                elif data['cmd'] == 'TOKEN':
                    login = data['login']
                    password = data['password']
                    self.current_user = yield User.by_login(
                        self.application.redis, login, password
                    )
                    if self.current_user:
                        res = {
                            'sender': 'SERVER',
                            'recipient': self.current_user.login,
                            'message': self.current_user.token
                        }
                        self.write_message(ujson.dumps(res))
                        raise Return()

            except (ValueError, KeyError):
                pass

            self.write_message('Wrong message')

    @coroutine
    def on_close(self):
        if self.current_user:
            self.application.ws.pop(self.current_user.login, None)
            yield self.current_user.deactivate(self.application.redis)
