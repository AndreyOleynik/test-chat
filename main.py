# coding: utf-8

import logging
import signal
import functools

import ujson
from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.gen import coroutine, Task
from tornadoredis import Client

from handlers import (
    IndexHandler,
    LoginHandler,
    WSHandler,
    TokenHandler
)
from settings import (
    COOKIE_SECRET,
    DIR_TEMPLATE,
    DEBUG,
    APP_PORT,
    APP_ADDRESS,
    REDIS_HOST,
    REDIS_PORT,
    REDIS_DB,
    REDIS_CHANNEL
)

logging.basicConfig()
logger = logging.getLogger(__name__)


class AppServer(Application):

    def __init__(self):
        self.ws = {}
        self._set_db()

        handlers = [
            (r"/", IndexHandler),
            (r"/login", LoginHandler),
            (r"/token", TokenHandler),
            (r"/ws", WSHandler),
        ]
        settings = {
            "login_url": "/login",
            "cookie_secret": COOKIE_SECRET,
            "debug": DEBUG,
            "compress_response": True,
            "template_path": DIR_TEMPLATE
        }
        super(AppServer, self).__init__(handlers, **settings)

    @coroutine
    def _set_db(self):
        self.redis = Client(REDIS_HOST, REDIS_PORT, selected_db=REDIS_DB)
        self.redis.connect()

        self.pubsub = Client(REDIS_HOST, REDIS_PORT)
        self.pubsub.connect()
        yield Task(self.pubsub.subscribe, REDIS_CHANNEL)
        self.pubsub.listen(self.on_message)

    def on_message(self, msg):
        if msg.kind == 'message':
            try:
                data = ujson.loads(msg.body)
                recipient = data['recipient']
                sender = data['sender']
                for k, v in self.ws.items():
                    if k != sender and recipient in ['ALL', k]:
                        v.write_message(msg.body)
            except (ValueError, KeyError):
                pass


def sig_handler(app, sig, frame):
    IOLoop.instance().add_callback(shutdown, app)


@coroutine
def shutdown(app):
    redis = app.redis
    for i in app.ws.values():
        yield i.current_user.deactivate(redis)
    IOLoop.instance().stop()
    logger.warning('SERVER STOPPED')

if __name__ == "__main__":
    app = AppServer()
    logger.warning('%sSERVER STARTED %s:%s' %
                   ('DEBUG ' if DEBUG else '', APP_ADDRESS, APP_PORT))
    signal.signal(signal.SIGTERM, functools.partial(sig_handler, app))
    signal.signal(signal.SIGINT, functools.partial(sig_handler, app))
    app.listen(APP_PORT, APP_ADDRESS)
    IOLoop.current().start()
