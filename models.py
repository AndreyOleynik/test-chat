# coding: utf-8

from tornado.gen import coroutine, Return
from tornado.gen import Task

from tools import random_str, make_psw
from settings import SESSION_TOKEN_SEC, APP_ID, TOKEN_LEN


class User(object):

    def __init__(self, login, salt, psw, token=None):
        self.login = login
        self.token = token
        self.salt = salt
        self.psw = psw

    def __str__(self):
        return '%s:%s' % (self.__class__.__name__, self.login)

    @staticmethod
    def _login_key(login):
        return 'user:%s' % login

    @staticmethod
    def _token_key(token):
        return 'token:%s' % token

    @coroutine
    def _check(self, password):
        return self.psw == make_psw(self.salt, password)

    @classmethod
    @coroutine
    def _new_token(cls, redis, login, old_token):
        if old_token:
            key = cls._token_key(old_token)
            yield Task(redis.delete, key)
        token = random_str(TOKEN_LEN)
        key = cls._token_key(token)
        yield Task(redis.set, key, login)
        yield Task(redis.expire, key, SESSION_TOKEN_SEC)
        raise Return(token)

    @classmethod
    @coroutine
    def by_login(cls, redis, login, password):
        login = login.strip()
        password = password.strip()

        if login and password:
            data = yield Task(redis.hmget, cls._login_key(login),
                              ['salt', 'psw', 'token'])
            if data['salt'] and data['psw']:
                if make_psw(data['salt'], password) != data['psw']:
                    raise Return(None)
            else:
                data['salt'] = random_str(100)
                data['psw'] = make_psw(data['salt'], password)

            data['token'] = yield cls._new_token(redis, login,
                                                 data.get('token'))
            self = cls(login=login, **data)
            yield Task(redis.hmset, cls._login_key(login), data)
            raise Return(self)

    @classmethod
    @coroutine
    def by_token(cls, redis, token):
        if token:
            login = yield Task(redis.get, cls._token_key(token))
            if login:
                data = yield Task(redis.hmget, cls._login_key(login),
                                  ['salt', 'psw', 'token'])
                if data['salt'] and data['psw']:
                    self = cls(login=login, **data)
                    raise Return(self)

    @coroutine
    def activate(self, redis):
        x = '%s:%s' % (APP_ID, self.login)
        yield Task(redis.sadd, 'active', x)

    @coroutine
    def deactivate(self, redis):
        x = '%s:%s' % (APP_ID, self.login)
        yield Task(redis.srem, 'active', x)

    @classmethod
    @coroutine
    def who(cls, redis):
        x = yield Task(redis.smembers, 'active')
        raise Return(list(set(i.split(':', 1)[1] for i in x)))
