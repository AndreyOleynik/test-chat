# coding: utf-8

import os.path

from tools import random_str


COOKIE_SECRET = "NeZabuduMatRodnyuZXSpectrumDorogoi"
SESSION_COOKIE_DAYS = 1
SESSION_TOKEN_SEC = 24 * 60 * 60

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
DIR_TEMPLATE = os.path.join(ROOT_DIR, 'template')

DEBUG = False
APP_PORT = 8888
APP_ADDRESS = "0.0.0.0"
APP_ID = random_str(5)

REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_DB = 0
REDIS_CHANNEL = "message"

TOKEN_LEN = 100
