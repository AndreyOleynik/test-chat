# coding: utf-8

import json
from urllib import urlencode
import datetime

from tornado.testing import AsyncHTTPTestCase, gen_test
from tornado.gen import Task, sleep, TimeoutError
from tornado.websocket import websocket_connect
from tornado.gen import coroutine, Return, with_timeout

from tools import random_str


class TestChatCase(AsyncHTTPTestCase):

    def setUp(self):
        super(TestChatCase, self).setUp()
        self._init_db()
        self.ws_url = 'ws://localhost:%s/ws' % self.get_http_port()

    @gen_test
    def _init_db(self):
        self.redis = self._app.redis
        res = yield Task(self.redis.keys, 'user:*')
        yield Task(self.redis.delete, *res)
        res = yield Task(self.redis.keys, 'token:*')
        yield Task(self.redis.delete, *res)
        yield Task(self.redis.delete, 'active')

    def get_app(self):
        from main import AppServer
        return AppServer()

    @coroutine
    def _make_ws(self, data, token=None):
        if token is None:
            body = urlencode(data)
            response = yield self.http_client.fetch(self.get_url('/token'),
                                                    method="POST", body=body)
            self.assertEqual(response.code, 200)
            token = response.body
        c = yield websocket_connect('%s?token=%s' % (self.ws_url, token))
        yield sleep(0.1)
        raise Return(c)

    def test_token1(self):
        data = {'login': random_str(10), 'password': random_str(10)}

        body = urlencode(data)
        response = self.fetch("/token", method="POST", body=body)
        token1 = response.body
        self.assertEqual(response.code, 200)
        self.assertIsNotNone(token1)

        response = self.fetch("/token", method="POST", body=body)
        token2 = response.body
        self.assertEqual(response.code, 200)
        self.assertIsNotNone(token2)
        self.assertNotEqual(token1, token2)

        self.redis.get('token:%s' % token1, callback=self.stop)
        login = self.wait()
        self.assertIsNone(login)

        self.redis.get('token:%s' % token2, callback=self.stop)
        login = self.wait()
        self.assertEqual(login, data['login'])

    def test_token2(self):
        data = {'login': random_str(10), 'password': random_str(10)}
        body = urlencode(data)
        response = self.fetch("/token", method="POST", body=body)
        self.assertEqual(response.code, 200)
        token = response.body

        data['password'] = random_str(10)
        body = urlencode(data)
        response2 = self.fetch("/token", method="POST", body=body)
        self.assertEqual(response2.code, 404)

        self.redis.get('token:%s' % token, callback=self.stop)
        login = self.wait()
        self.assertEqual(login, data['login'])

    @gen_test
    def test_ws_who1(self):
        msg = json.dumps({"cmd": "WHO"})

        data1 = {'login': random_str(10), 'password': random_str(10)}
        c1 = yield self._make_ws(data1)

        c1.write_message(msg)
        response = json.loads((yield c1.read_message()))
        msg2 = {
            'message': [data1['login']],
            'recipient': data1['login'],
            'sender': 'SERVER'
        }
        self.assertDictEqual(response, msg2)

        data2 = {'login': random_str(10), 'password': random_str(10)}
        c2 = yield self._make_ws(data2)

        c2.write_message(msg)
        response = json.loads((yield c2.read_message()))
        response['message'].sort()
        msg2 = {
            'message': [data1['login'], data2['login']],
            'recipient': data2['login'],
            'sender': 'SERVER'
        }
        msg2['message'].sort()
        self.assertDictEqual(response, msg2)

        data3 = {'login': random_str(10), 'password': random_str(10)}
        c3 = yield self._make_ws(data3)

        c3.write_message(msg)
        response = json.loads((yield c3.read_message()))
        response['message'].sort()
        msg2 = {
            'message': [data1['login'], data2['login'], data3['login']],
            'recipient': data3['login'],
            'sender': 'SERVER'
        }
        msg2['message'].sort()
        self.assertDictEqual(response, msg2)

        c2.write_message(msg)
        response = json.loads((yield c2.read_message()))
        response['message'].sort()
        msg2 = {
            'message': [data1['login'], data2['login'], data3['login']],
            'recipient': data2['login'],
            'sender': 'SERVER'
        }
        msg2['message'].sort()
        self.assertDictEqual(response, msg2)

    @gen_test
    def test_ws_who2(self):
        msg = json.dumps({"cmd": "WHO"})

        data1 = {'login': random_str(10), 'password': random_str(10)}
        c1 = yield self._make_ws(data1)

        c1.write_message(msg)
        response = json.loads((yield c1.read_message()))
        msg2 = {
            'message': [data1['login']],
            'recipient': data1['login'],
            'sender': 'SERVER'
        }
        self.assertDictEqual(response, msg2)

        c2 = yield self._make_ws(data1, random_str(10))
        c2.write_message(msg)
        response = yield c2.read_message()
        self.assertIsNone(response)

        c1.write_message(msg)
        response = json.loads((yield c1.read_message()))
        msg2 = {
            'message': [data1['login']],
            'recipient': data1['login'],
            'sender': 'SERVER'
        }
        self.assertDictEqual(response, msg2)

    @gen_test
    def test_ws_message1(self):
        data1 = {'login': random_str(10), 'password': random_str(10)}
        c1 = yield self._make_ws(data1)

        data2 = {'login': random_str(10), 'password': random_str(10)}
        c2 = yield self._make_ws(data2)

        data3 = {'login': random_str(10), 'password': random_str(10)}
        c3 = yield self._make_ws(data3)

        msg1 = {
            'cmd': 'MSG',
            'sender': data1['login'],
            'recipient': 'ALL',
            'message': random_str(10)
        }
        c1.write_message(json.dumps(msg1))

        msg2 = json.loads((yield c2.read_message()))
        self.assertDictEqual(msg1, msg2)

        msg3 = json.loads((yield c3.read_message()))
        self.assertDictEqual(msg1, msg3)

    @gen_test(timeout=10)
    def test_ws_message2(self):
        td = datetime.timedelta(seconds=1)
        data1 = {'login': random_str(10), 'password': random_str(10)}
        c1 = yield self._make_ws(data1)

        data2 = {'login': random_str(10), 'password': random_str(10)}
        c2 = yield self._make_ws(data2)

        data3 = {'login': random_str(10), 'password': random_str(10)}
        c3 = yield self._make_ws(data3)

        msg1 = {
            'cmd': 'MSG',
            'sender': data1['login'],
            'recipient': data2['login'],
            'message': random_str(10)
        }
        c1.write_message(json.dumps(msg1))

        msg2 = json.loads((yield c2.read_message()))
        self.assertDictEqual(msg1, msg2)

        with self.assertRaises(TimeoutError):
            yield with_timeout(td, c3.read_message())

        with self.assertRaises(TimeoutError):
            yield with_timeout(td, c1.read_message())

    @gen_test(timeout=10)
    def test_ws_message3(self):
        td = datetime.timedelta(seconds=1)
        data1 = {'login': random_str(10), 'password': random_str(10)}
        c1 = yield self._make_ws(data1)

        data2 = {'login': random_str(10), 'password': random_str(10)}
        c2 = yield self._make_ws(data2)

        data3 = {'login': random_str(10), 'password': random_str(10)}
        c3 = yield self._make_ws(data3)

        msg1 = {
            'cmd': 'MSG',
            'sender': data1['login'],
            'recipient': random_str(10),
            'message': random_str(10)
        }
        c1.write_message(json.dumps(msg1))

        res = yield c1.read_message()
        self.assertEqual(res, 'Wrong message')

        with self.assertRaises(TimeoutError):
            yield with_timeout(td, c2.read_message())

        with self.assertRaises(TimeoutError):
            yield with_timeout(td, c3.read_message())

    @gen_test(timeout=10)
    def test_ws_message4(self):
        td = datetime.timedelta(seconds=1)
        data1 = {'login': random_str(10), 'password': random_str(10)}
        c1 = yield self._make_ws(data1)

        data2 = {'login': random_str(10), 'password': random_str(10)}
        c2 = yield self._make_ws(data2)

        data3 = {'login': random_str(10), 'password': random_str(10)}
        c3 = yield self._make_ws(data3)

        msg1 = {
            'cmd': 'MSG',
            'sender': random_str(10),
            'recipient': 'ALL',
            'message': random_str(10)
        }
        c1.write_message(json.dumps(msg1))

        res = yield c1.read_message()
        self.assertEqual(res, 'Wrong message')

        with self.assertRaises(TimeoutError):
            yield with_timeout(td, c2.read_message())

        with self.assertRaises(TimeoutError):
            yield with_timeout(td, c3.read_message())
