# coding: utf-8

import string
import random
import binascii
from hashlib import pbkdf2_hmac


def random_str(n):
    s = string.ascii_letters + string.digits
    return ''.join(random.choice(s) for _ in range(n))


def make_psw(salt, password):
    return binascii.hexlify(pbkdf2_hmac('sha256', password[:100], salt, 500))
